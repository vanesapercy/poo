package poo;

class Empleados {

	// Campos de clase o propiedades
	// private final String nombre;
	private String nombre;
	private String seccion;
	private int id;
	//Variable estática
	private static int idSiguiente = 1 ;

	public Empleados() {

		//nombre = "Ana";

	}

	public Empleados(String nombre) {

		this.nombre = nombre;
		id = idSiguiente;
		idSiguiente++;
	}

	public void setSeccion(String seccion) {

		this.seccion = seccion;

	}

	public String getNombre() {

		return nombre;
	}

	public String getSeccion() {

		return seccion;
	}

	public String getDatosEmpleado() {

		return "El empleado " + nombre + " pertenece a la sección de  " + seccion + " y tiene el número " + id;
	}
	
	//Método estático
	public static String getIdSiguiente() {
		
		return "El ID del siguiente empleado será: " + idSiguiente;
	}

}
