package poo;

import java.util.Random;



class CuentaCorriente {

	private  double saldo;
	private  String nombreTitular;
	private long numeroCuenta;

	// Constructor
	public CuentaCorriente(String nombreTitular, double saldo) {

		this.nombreTitular = nombreTitular;
		this.saldo = saldo;
		Random rnd = new Random();
		numeroCuenta = Math.abs(rnd.nextLong());

	}

	public void setIngreso(double ingreso) {

		if (ingreso < 0)
			System.out.println("No se permiten ingresos negativos");

		else
			saldo += ingreso;
	}

	public void setReintegro(double reintegro) {

		saldo -= reintegro;

	}

	public String getSaldo() {
		
		return "El saldo de la cuenta es: " + saldo;
	}
	
	public static void Transferencia (CuentaCorriente origen, CuentaCorriente destino, double cantidad) {
		
		origen.saldo -= cantidad;
		destino.saldo += cantidad;
		
	}
	
	public String getDatosCuenta() {
		
		return "Titular: " + nombreTitular + "\n" +
		"N° de Cuenta: " + numeroCuenta + " \n" +
				"Saldo: " + saldo;
 	}

}
