package poo;

//Objeto Vehiculos
public class Vehiculos {

	// Propiedades
	private int ruedas;
	private int largo;
	private int ancho;
	private String color;
	private int peso;
	private boolean climatizador;
	private boolean gps;
	private boolean tapiceriaCuero;

	// Constructor - Hay sobrecarga de constructores
	public Vehiculos() {

		ruedas = 4;
		largo = 2;
		ancho = 2;
		color = "Sin color";

	}

	public Vehiculos(int ruedas) {

		this.ruedas = ruedas;
		largo = 2;
		ancho = 1;
		peso = 2;
		color = "Sin color";

	}

	public Vehiculos(int ruedas, int largo, double ancho, int peso) {
		
		this.ruedas = ruedas;
		this.largo = largo;
		this.ancho = (int) ancho;
		this.peso = peso;

	}

	// Métodos

	// Método Setter para establecer el valor de propiedad color - sin parámetros
	public void setColor() {
		color = "Azul";
	}

	public String getColor() {

		return color;
	}

	// Método Setter establece un valor para la propiedad color
	public void setColor(String colorVehiculo) {

		color = colorVehiculo;
	}

	public void setExtra(boolean climatizador) {

		this.climatizador = climatizador;
	}

	public void setExtra(boolean climatizador, boolean gps, boolean tapiceriaCuero) {

		this.climatizador = climatizador;
		this.gps = gps;
		this.tapiceriaCuero = tapiceriaCuero;
	}

}

class Uso_Vehiculos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Instanciando un objeto de la clase Vehiculos
		Vehiculos miCoche = new Vehiculos();

		// Accediendo al comportamiento del objeto
		// miCoche.setColor();

		miCoche.setColor("Amarillo");

		System.out.println(miCoche.getColor());

	}

}
