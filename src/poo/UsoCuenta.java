package poo;

class UsoCuenta{
	
	public static void main(String [] args) {
		
		CuentaCorriente Cuenta1 = new CuentaCorriente("Lucía Giraldo", 1500);
		
		CuentaCorriente Cuenta2 = new CuentaCorriente("Paco de Lucía", 1500);
		
		CuentaCorriente.Transferencia(Cuenta1, Cuenta2, 200);
		
		System.out.println(Cuenta1.getDatosCuenta());
		System.out.println(Cuenta2.getDatosCuenta());
		
		
	}
}
